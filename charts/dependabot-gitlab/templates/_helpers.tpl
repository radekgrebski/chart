{{/*
Expand the name of the chart.
*/}}
{{- define "dependabot-gitlab.name" -}}
{{ include "common.names.name" . }}
{{- end }}

{{/*
Create a default fully qualified app name.
*/}}
{{- define "dependabot-gitlab.fullname" -}}
{{ include "common.names.fullname" . }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "dependabot-gitlab.chart" -}}
{{ include "common.names.chart" . }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "dependabot-gitlab.labels" -}}
{{ include "common.labels.standard" . }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "dependabot-gitlab.selectorLabels" -}}
{{ include "common.labels.matchLabels" . }}
{{- end }}

{{/*
Redis name
*/}}
{{- define "dependabot-gitlab.redisName" -}}
{{- $redis := dict "Values" .Values.redis "Chart" (dict "Name" "redis") "Release" .Release -}}
{{ include "common.names.fullname" $redis }}
{{- end }}

{{/*
Mongodb name
*/}}
{{- define "dependabot-gitlab.mongodbName" -}}
{{- $mongodb := dict "Values" .Values.mongodb "Chart" (dict "Name" "mongodb") "Release" .Release -}}
{{ include "common.names.fullname" $mongodb }}
{{- end }}

{{/*
Pod annotations
*/}}
{{- define "dependabot-gitlab.podAnnotations" -}}
checksum/secrets: {{ include (print $.Template.BasePath "/secrets.yaml") . | sha256sum }}
checksum/config: {{ include (print $.Template.BasePath "/configmap.yaml") . | sha256sum }}
{{- if and .Values.redis.auth.enabled (not .Values.redis.existingSecret) }}
checksum/redis-password: {{ default (randAlphaNum 10) .Values.redis.auth.password | sha256sum }}
{{- end }}
{{- if and .Values.mongodb.auth.enabled (not .Values.mongodb.existingSecret) }}
checksum/mongodb-password: {{ default (randAlphaNum 10) .Values.mongodb.auth.password | sha256sum }}
{{- end }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "dependabot-gitlab.serviceAccountName" -}}
{{- if .Values.serviceAccount.create -}}
  {{ default (include "dependabot-gitlab.fullname" .) .Values.serviceAccount.name }}
{{- else -}}
  {{ default "default" .Values.serviceAccount.name }}
{{- end -}}
{{- end -}}

{{/*
Environment config
*/}}
{{- define "dependabot-gitlab.database-credentials" -}}
{{- if and .Values.redis.enabled .Values.redis.auth.enabled -}}
- name: REDIS_PASSWORD
  valueFrom:
    {{- $redis := dict "Values" .Values.redis "Chart" (dict "Name" "redis") "Release" .Release }}
    secretKeyRef:
      name: {{ include "redis.secretName" $redis }}
      key: {{ include "redis.secretPasswordKey" $redis }}
{{- end }}
{{- if and .Values.mongodb.enabled .Values.mongodb.auth.enabled }}
- name: MONGODB_PASSWORD
  valueFrom:
    {{- $mongodb := dict "Values" .Values.mongodb "Chart" (dict "Name" "mongodb") "Release" .Release "Template" .Template }}
    secretKeyRef:
      name: {{ include "mongodb.secretName" $mongodb }}
      key: mongodb-passwords
{{- end }}
{{- end }}

{{/*
Projects
*/}}
{{- define "dependabot-gitlab.projects" -}}
{{- join " " .Values.projects }}
{{- end }}

{{/*
Image data
*/}}
{{- define "dependabot-gitlab.image" -}}
image: "{{ .Values.image.repository }}:{{ .Values.image.tag | default .Chart.AppVersion }}"
imagePullPolicy: {{ .Values.image.pullPolicy }}
{{- end }}

{{/*
Migration job wait container
*/}}
{{- define "dependabot-gitlab.migrationsWaitContainer" -}}
- name: wait-migrations
  {{- include "dependabot-gitlab.image" . | nindent 2 }}
  args:
    - "rake"
    - "dependabot:check_migrations"
  {{- with (include "dependabot-gitlab.database-credentials" .) }}
  env:
    {{- . | nindent 4 }}
  {{- end }}
  envFrom:
    - configMapRef:
        name: {{ include "dependabot-gitlab.fullname" . }}
    - secretRef:
        name: {{ include "dependabot-gitlab.fullname" . }}
{{- end }}


{{/*
Redis wait container
*/}}
{{- define "dependabot-gitlab.redisWaitContainer" -}}
- name: wait-redis
  {{- include "dependabot-gitlab.image" . | nindent 2 }}
  args:
    - "rake"
    - "dependabot:check_redis"
  {{- with (include "dependabot-gitlab.database-credentials" .) }}
  env:
    {{- . | nindent 4 }}
  {{- end }}
  envFrom:
    - configMapRef:
        name: {{ include "dependabot-gitlab.fullname" . }}
    - secretRef:
        name: {{ include "dependabot-gitlab.fullname" . }}
{{- end }}
