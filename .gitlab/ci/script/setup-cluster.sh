#!/bin/bash

set -euo pipefail

source "$(dirname "$0")/utils.sh"

log "Create '$NAMESPACE' namespace"
kubectl create namespace "$NAMESPACE"

log "Create serviceMonitor CRD"
kubectl create -f https://raw.githubusercontent.com/prometheus-operator/kube-prometheus/main/manifests/setup/0servicemonitorCustomResourceDefinition.yaml

log "Install gitlab mock"
echo "** Adding repo **"
helm repo add andrcuns https://andrcuns.github.io/charts

echo "** Installing smocker chart **"
helm install gitlab andrcuns/smocker \
  -n "$NAMESPACE" \
  -f .gitlab/ci/kube/mock-definitions.yaml \
  --timeout 2m \
  --atomic

if [[ "$VALUES_FILE" == "secrets-values.yaml" ]]; then
  log "Create custom secrets for credentials"
  kubectl create -f .gitlab/ci/kube/secrets.yaml -n "$NAMESPACE"
fi

log "Prepare '${VALUES_FILE}' file"
cp ".gitlab/ci/kube/${VALUES_FILE}" charts/dependabot-gitlab/ci/
cat <<YML >>"charts/dependabot-gitlab/ci/${VALUES_FILE}"
env:
  gitlabUrl: http://gitlab-smocker.${NAMESPACE}.svc.cluster.local:8080
YML
