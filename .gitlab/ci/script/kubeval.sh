#!/bin/bash

set -euo pipefail

source "$(dirname "$0")/utils.sh"

log "Update chart dependencies"
helm dependency update $CHART_DIR

for yaml in .gitlab/ci/kube/*-values.yaml; do
  message="Validating $CHART_DIR with $yaml"
  length=$(echo "$message" | awk '{print length}')
  delimiter=$(head -c $length </dev/zero | tr '\0' '=')

  log "$delimiter"
  log "$message"
  log "$delimiter"

  helm template $CHART_DIR -f $yaml | kubeval \
    --strict \
    --schema-location https://raw.githubusercontent.com/yannh/kubernetes-json-schema/master \
    --additional-schema-locations https://raw.githubusercontent.com/joshuaspence/kubernetes-json-schema/master
done
